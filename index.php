<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" type="text/css"/> 
    <title>Lab4Timin</title>
</head>
<body class="body">
<header class="header">
    <?php
        function input($input)
        {
            $splittedString = explode(' ', $input);
            $operand = $splittedString[1];
            $variable = array_search('x', $splittedString);
            if ($variable == 0) return calc($splittedString[4], $splittedString[2], $operand);
            if ($variable == 2) return calc($splittedString[4], $splittedString[0], $operand);
        }
        function calc($first, $second, $operand)
        {
            if ($operand == '+') return $first - $second;
            if ($operand == '-') return $first + $second;
            if ($operand == '/') return $first * $second;
            if ($operand == '*') return $first / $second;
        }
        echo input('х - 5 = 1');
    ?>
</header>